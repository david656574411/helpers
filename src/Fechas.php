<?php
namespace Davidgarcia\Helpers;

use DateInterval;
use DateTime;

class Fechas
{
    /**
     * @throws \Exception
     */
    public function calcularFechasRecordar(string $date, int $dias, int $recordatorios, bool $constante): array
    {

        $fecha = new DateTime($date);

        $rs = (range($dias, $dias * $recordatorios, $dias));
        $arrayFecha = [];
        if ($constante) {
            foreach ($rs as $r) {
                $arrayFecha[] = $fecha->add(new DateInterval('P' . $dias . 'D'))->format('d-m-Y');
            }
        } else {
            foreach ($rs as $r) {
                $arrayFecha[] = $fecha->add(new DateInterval('P' . $r . 'D'))->format('d-m-Y');
            }

        }

        return $arrayFecha;

    }

}